#!/bin/bash
PROJECT_NAME=$1
mv -f ci/docker-compose.override.yml docker-compose.override.yml
git checkout .env
echo JENKINS_USER_ID=`id -u` >> .env
echo JENKINS_GROUP_ID=`id -g` >> .env
echo PROJECT_NAME=$PROJECT_NAME >> .env
docker-compose up -d --build
# Sleep a few seconds to let docker launch internal services:
sleep 10
