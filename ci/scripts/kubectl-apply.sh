#!/bin/bash
kubectl apply -f /deploy/*.yml
kubectl get secret regcred --namespace=default --export -o yaml | kubectl apply --namespace=${CI_PROJECT_NAME}-prod -f -
