#!/bin/bash
export AUTH=$(htpasswd -nb  $1 $2  | base64)
ls -l -a
mkdir ./kubernetes/.generated
for f in ./kubernetes/deploy-cerebro_backend.pro*.yml
do
    envsubst < $f > "./kubernetes/.generated/$(basename $f)"
done
cat ./kubernetes/.generated/deploy-cerebro_backend.prod.yml
