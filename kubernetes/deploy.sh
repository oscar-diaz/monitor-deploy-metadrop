#!/usr/bin/env bash

export CI_PROJECT_NAME=cerebro-ok
export DOMAIN=metadrop.org
export SUBDOMAIN=cerebro.granada
export CONTAINER_RELEASE_IMAGE_NGINX=metadrop/cerebro-nginx:0.10
export CONTAINER_RELEASE_IMAGE_PHP=metadrop/cerebro-php:0.10
export CONTAINER_RELEASE_IMAGE_CEREBRO_V2=metadrop/cerebro-angular:0.59
export USERNAME=metadrop
export PASSWORD=metadropci
export AUTH=$(htpasswd -nb  $USERNAME $PASSWORD  | base64)
for f in ./*.prod.yml
do
  envsubst < $f > "./.generated/$(basename $f)"
done
ls -l ./.generated 
cat ./.generated/deploy-cerebro_v2.prod.yml
kubectl apply -f ./.generated/

